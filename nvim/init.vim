"      ___                                       ___     
"     /\  \         _____         _____         /\__\    
"     \:\  \       /::\  \       /::\  \       /::|  |   
"      \:\  \     /:/\:\  \     /:/\:\  \     /:/:|  |   
"  _____\:\  \   /:/ /::\__\   /:/ /::\__\   /:/|:|  |__ 
" /::::::::\__\ /:/_/:/\:|__| /:/_/:/\:|__| /:/ |:| /\__\
" \:\~~\~~\/__/ \:\/:/ /:/  / \:\/:/ /:/  / \/__|:|/:/  / nvim config
"  \:\  \        \::/_/:/  /   \::/_/:/  /      |:/:/  / 
"   \:\  \        \:\/:/  /     \:\/:/  /       |::/  /  
"    \:\__\        \::/  /       \::/  /        |:/  /   
"     \/__/         \/__/         \/__/         |/__/    

" pathogen
execute pathogen#infect()
syntax on
filetype plugin indent on

" enable syntax highlighting
syntax enable

" true color
set termguicolors

" colorscheme
set background=dark
let g:blackspace_italics=1
colorscheme blackspace
" Gruvbox
"let g:gruvbox_italic=1
"autocmd vimenter * ++nested colorscheme gruvbox

" line numbers
set nu

" add mouse funcionality
set mouse=a

" highlight current row and column
set cursorline
set cursorcolumn

" new line without insert mode with enter
nmap <S-Enter> 0<Esc>
nmap <CR> o<Esc>

" clear highlighting for search
nmap <F3> :noh<CR>

" fold method
set foldmethod=marker

" Rasi syntax highlighting
au BufNewFile,BufRead /*.rasi setf css
